CMS
***

**I don't think this app does anything useful any more!**

Django application for a simple CMS site

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3.5 venv-cms
  source venv-cms/bin/activate
  pip install --upgrade pip

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
