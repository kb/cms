-r base.txt
-e .
-e ../base
-e ../block
-e ../compose
-e ../login
-e ../mail
devpi-client
fabric
factory-boy
psycopg2
pytest-cov
pytest-django
pytest-flakes
pytest-pep8
